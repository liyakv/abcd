from flask import Flask,render_template

app = Flask(__name__)


@app.route('/home')
def homepage():
    return render_template('admin home.html')
@app.route('/login')
def loginpage():
    return render_template('admin login.html')
@app.route('/viewusers')
def viewuserspage():
    return render_template('admin view users.html')
@app.route('/newsview')
def newsviews():
    return render_template('news view.html')
@app.route('/upload')
def updatepage():
    return render_template('newsupload.html')




if __name__ == '__main__':
    app.run()
